#include<stdio.h>
#include "aakash_functions.h"

int str_length( char *str)  //finds length of string
{
	int i=0;
	
	while( *str != '\0')
	{
		str++;
		i++;
	}

	if(i>0) {
		return i;
	}else {
		return -1;
	}
	
}



int str_copy(char *str1, char *str2)  //returns 0 on successful copying of string
{
	if(*str2 != '\0') 
	{
		while( *str2 != '\0')
		{
		*str1 = *str2;
		str2++;
		str1++;
		}
		*str1 = '\0';
		
		return 0;

	}else {
		return -1;
	}
	
}


int str_compare(char *str1, char *str2) //if str1 is less than str2 the function  return -1 if str1 is greater than str2 it  return 1 and if both are equal then return 0
{
	int i=0;
	

	while ( str1[i] != '\0' || str2[i] != '\0')
	{
		 if( str1[i] < str2[i] )
		{
			return -1;
		}else if( str1[i] > str2[i] )
		{
			return 1;
		}

		i++;

	}

	
 
		return 0;
			
}



int str_find_char(char *str, char *ch)  //returns position of character in the string
{
	int i=0, k=0;
	while ( str[i] != '\0')
	{
		if( str[i] == *ch )
		{
			k++;
			break;
		}

		i++;

	}

	if( k>0) return i;
	else return -1;

}


int str_find_substring(char *str1, char *str2)  //returns position of small string in the large string
{
	int i=0, j=0;
	

	while( str1[i] != '\0' )
	{
		if(str2[j] == '\0' ) 

		{
			break;
		}

		if( str2[j] == str1[i])
		{
			j++;
		}else{
			j=0;
		}
		i++;
	}

	if(j>'\0')
	{
		return i-j;
	}else{
		return -1;
	}
}