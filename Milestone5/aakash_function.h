#ifndef aakash_function
#define aakash_function


int str_length( char *str);

int str_copy(char *str1, char *str2);

int str_compare(char *str1, char *str2);

int str_find_char(char *str, char *ch);

int str_find_substring(char *str1, char *str2);




#endif
