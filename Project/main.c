#include<stdio.h>
#include"comm_xtract.h"
int main()
{
	char s1[100],s2[100];   //S1 to store user speech and S2 to extract actual command
	int a,b,i=0,j;
	
	printf("Enter command\n \n");
	scanf(" %[^\n]s", s1);    // Storing  user speech  
	a= pos(s1);                // To recognize keyword go/Go
	printf("Position of first character is %d\n", a);   // Position of the first Character of actual command 
	if(a!=-1)                 // This statement is executed if there is keyword in the speech
	{
		
		b=str_length(s1,&a);  //  To find the length of actual comand 
		printf("Length of actual command is %d \n",b);
	}
	else 
	{
		printf("wrong command");// This statement is executed if there is no keyword in the speech
		return -1;
	}
	
	j=0;
	
	for(i=a;i<(a+b);i++)      // Extracting actual command form speech
	{
		s2[j]=s1[i];
		j++;
	}
	
	
	s2[j++]='\0';
	
	
		printf("Printing actual command:- \t %s", s2);  // Printing actual command 
	
	
return 0;


}