#include <stdio.h>
#include <stdlib.h>

int main()
{

    float in_cm;
    float  in_inches;

    printf("Inches: ");
    scanf("%f", &in_inches);

    in_cm = in_inches*2.54;

    printf("CM =  %.2f", in_cm);
    return(0);

}