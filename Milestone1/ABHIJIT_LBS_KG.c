#include <stdio.h>
#define poundtokg 0.453592
int main() 
{
        float pound, kilogram;
        printf("Enter weight in pound:");
        
        scanf("%f", &pound);
        kilogram = pound * poundtokg;

        printf("%.2f pound = %.2f KiloGram\n", pound, kilogram);
        return 0;
  }