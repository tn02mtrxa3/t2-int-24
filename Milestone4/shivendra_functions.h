#ifndef SHIVENDRA_FUNCTIONS
#define SHIVENDRA_FUNCTIONS

int add_element(int x[]);

int search_element(int x[]);

int remove_element(int x[]);

int sort_array( int x[], int n);

int print_array(int x[]);




#endif