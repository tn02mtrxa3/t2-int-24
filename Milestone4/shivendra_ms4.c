#include<stdio.h>
#include "shivendra_functions.h"

int count=10; //global variable to track the size of array


int main()
{
	int i, a, b;
	int x[20];
	

	printf("\nEnter 10 numbers\n");
	

	for(i=0;i<10;i++)
	{
	scanf("%d", &x[i]);
	}	
	

	do{
	printf("\n\nSelect the option:\n1.Add an element to the Array\n2.Search if an element exists in the array\n3.Remove an element from the array\n4.Sort the array\n5.Print all elements in the array\n6.Exit\n");
	scanf(" %d", &a);


	switch(a){

		case 1 : add_element( x);
				 break;


		case 2 : search_element( x);
				 break;


		case 3 : remove_element( x);
				 break;


		case 4 : printf("\nSelect 1 for ascending \t 2 for descending\n");
				 scanf(" %d", &b);
				 sort_array( x, b);
				 break;


		case 5 : 
				
				 print_array( x);

				 break;


		case 6 : break;

		default : printf("\nInvalid Option. Please try again.");



	}


	}while(a!=6);

	return 0;
}