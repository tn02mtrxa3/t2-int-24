#include "shivendra_math_functions.h"
#include <stdio.h>


double power(double x, int n)
{
	double ans=1;
	int i;
	
	if(n!=0){
		for(i=1;i<=n;i++)
		{
			ans=ans*x;
		}

		return ans; //if power is greater than 1
	}else{
		return 1; //if power is 0
	}


}





int factor(int a)
{
	int i;

	printf("\n\nFactors of the number %d : ", a);

	for(i=1;i<=a;i++)
	{
		if(a%i==0)
		{
			printf("\t%d ", i);
			
		}
	}

	return 0;
}


int prime_fact(int a)
{
	int i, j, k=0;
	printf("\n\nPrime Factors of the number %d : ", a);
	for(i=2;i<=a;i++)
	{
		if(a%i==0)
		{
			for (j=1;j<=i;j++)
				{
					if(i%j==0)
					{	

						k++;
					}
				}
		}
		if(k==2)
			{
			printf("\t%d ", i);
			}
		k=0;
	}

	return 0;

}


int is_prime(unsigned int x2)
{

	int j, k=0;

	if(x2==1) 
		{
		printf("\t1 is neither prime nor composite");
	
	}else if (x2==0) 
		{
			printf("\tEnter number greater than 1");
	}else if(x2>=1)
		{

		for (j=1;j<=x2;j++)
				{
					if(x2%j==0)
					{	

						k++;
					}
				}
		}
		if(k==2)
			{
			return 1;
			}else {
			return 0;
			}
	
}


int gcd(int a, int b)
{
	int i, z, x;

	
	if(a>=b)
	{
	z=a;
	for(i=1;i<=z;i++)
	{
		if(a%i==0 && b%i==0)
		{
			x=i;
		}
	}
	
	}else {
	z=b;
	for(i=1;i<=z;i++)
	{
		if(a%i==0 && b%i==0)
		{
			x=i;
		}
	}

	}
	

	return x;
}


double fv(double rate, unsigned int nperiods, double PV)
{
 
double  z=1;
int i;

if(nperiods>0)
	{
		for(i=1;i<=nperiods;i++)
		{
		z=z*(1+rate);
		}

		return PV*z;
	}else{
	return 0;
	}
}




double pv(double rate, unsigned int nperiods, double FV)

{
 
double z=1;
int i;

if(nperiods>0)
	{
		for(i=1;i<=nperiods;i++)
		{
		z=z*(1+rate);
		}

		return FV/z;
	}else{
	return 0;
	}
}

int oddeven (int a)
{
   
   if(a%2==0){
   	return 0; //for even
   }else{
   	return 1; //for odd
   }

}


int lcm3 (int a, int b, int c)
{
	int i, gcd1=0, gcd2=0, lcm1=0, lcm2=0;

	for(i=1;i<=a;i++)
	{
		if(a%i==0 && b%i==0)
		{
			gcd1=i;
			
		}
	
	}
	lcm1=(a*b)/gcd1;

	for(i=1;i<=c;i++)
	{
		if(c%i==0 && lcm1%i==0)
		{
			gcd2=i;
			
		}
	
	
	}
	lcm2=(lcm1*c)/gcd2;
	return lcm2;
}



unsigned int fact (unsigned int n)
{
	unsigned int x2=1;
	int i;

	if(n>0)
	{
		for(i=1;i<=n;i++)
		{
		x2=i*x2;
		}
		return x2;
	}else{
		return 1;
	}
}



void fibonacci (int a)
{
	int z=1, i, x=0, y=1;

	if(a==1)
	{
		printf("\t0");
	}else{
			printf("\t0\t1");
		for(i=1;i<a;i++)
		{
			z=x+y;
			printf("\t%u", z);
			x=y;
			y=z;
		}
	
		}
}