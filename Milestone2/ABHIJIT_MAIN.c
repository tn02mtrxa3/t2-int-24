#include<stdio.h>
#include "ABHIJIT_FUNCTIONS.h"

int main()
{

	int number, a, b, c;
	double x1;
	int n1;
	unsigned int x2, n, r;
	double rate; 
	unsigned int nperiods; 
	double PV, FV;
	
	

	do{

		printf("\n\nType the number corresponding to the math function which you want to perform:\n0. To exit\n1. To calculate n'th power of x\n2. To calculate greatest common divisor of a and b\n3. To know given number is prime or not\n4. To calculate future value of an investment\n5. To calculate present value of an investment\n6. To find all factors of a number\n7. To find all prime factors of a number\n8. To check odd or even\n9. To get LCM of three numbers\n10. To calculate factorial of n\n11. nCr combination\n12. nPr permutation\n13. To print first n fibonacci numbers\n");

		scanf(" %d", &number);

		switch(number){
			case 0 : break;
			
			case 1 : printf("\n\nEnter x and n \n");
					 scanf(" %lf %d", &x1, &n1);
					 power( x1, n1);
					 break;
			
			case 2 : printf("\n\nEnter a and b \n");
 					 scanf(" %d %d", &a, &b);
 					 gcd( a, b);
					 break;
			
			case 3 : printf("\n\nEnter number to check wether it is prime or not (If it is prime number it will print '1' else '0': ");
					 scanf(" %u", &x2);
					 is_prime(x2);
					 break;
			
			case 4 :printf("\n\nTo find FV : \t Enter value of rate, nperiods, present value"),
					scanf(" %lf %u %lf", &rate, &nperiods, &PV);
					fv( rate, nperiods, PV);
					break;
			
			case 5 :printf("\n\nTo find PV : \t Enter value of rate, nperiods, future value"),
					scanf(" %lf %u %lf", &rate, &nperiods, &FV);
					pv( rate, nperiods, FV);
					break;
			
			case 6 : printf("\nEnter number :\n");
					 scanf(" %d", &a);
					 factor(a);
					 break;
			
			case 7 :  printf("\nEnter number :\n");
					 scanf(" %d", &a);
					 prime_fact(a);
					 break;
			
			case 8 :printf("\nEnter the number\n");
					scanf("%d", & a);
					oddeven ( a);
					break;
			
			case 9 :printf("\n\nEnter value of a, b, c in decreasing order");
					scanf("%d %d %d", &a, &b, &c);
					lcm3 ( a, b, c);
					break;
			
			case 10 :printf("\n\nEnter number to find factorial");
					 scanf("%u", &x2);
					 fact(x2);
					 break;
			
			case 11 :printf("\n\nEnter value of n and r");
					 scanf(" %u %u", &n, &r);
					 printf("\n\n nCr : \t%u", (fact(n))/(fact(r)*fact(n-r)));
					 break;
			
			case 12 :printf("\n\nEnter value of n and r");
					 scanf(" %u %u", &n, &r);
					 printf("\n\n nPr : \t%u", (fact(n))*(fact(n-r)));
					 break;
			
			case 13 :printf("\n\nEnter number upto which you want fibonacci series");
					 scanf(" %d", &x2);
					 fibo(x2);
					 break;

			default : printf("Please enter valid number corresponding to the math function which you want to perform:\n");


		}

	}while(number!=0);






	return 0;


}