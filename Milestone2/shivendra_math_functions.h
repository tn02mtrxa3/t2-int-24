#ifndef SHIVENDRA_MATH_FUNCTIONS
#define SHIVENDRA_MATH_FUNCTIONS


double power(double x, int n);

int factor(int a);

int prime_fact(int a);

int is_prime(unsigned int x);

int gcd(int a, int b);

double fv(double rate, unsigned int nperiods, double PV);

double pv(double rate, unsigned int nperiods, double FV);

int oddeven (int a);

int lcm3 (int a, int b, int c);

unsigned int fact (unsigned int n);

void fibonacci (int a);

#endif